package ru.dev.calendar;
import android.content.pm.PackageInfo;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

public class ActivityMain extends AppCompatActivity implements DialogDayOptions.OnDialogDayOptionsResultListener {

    public final int COUNT_DAYS_IN_WEEK = 7;
    private TextView tvViewDay;
    private GridViewSquareCells gvCalendar;

    final String NEXT_MONTH = "next_month";
    final String PREV_MONTH = "prev_month";

    final int MENU_ABOUT = 1;

    final ViewGroup parentNull = null;

    private DialogDayOptions dialogDayOptions;

    private static ActivityMain instance;

    static ActivityMain getInstance() {
        return instance;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            try {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_main);

                this.instance = this;

                try {
                    assert getSupportActionBar() != null;
                    // don't show header of activity
                    getSupportActionBar().setDisplayShowTitleEnabled(false);

                    // view icons on top panel
                    getSupportActionBar().setDisplayShowHomeEnabled(true);
                    getSupportActionBar().setLogo(R.mipmap.ic_flower);
                    getSupportActionBar().setDisplayUseLogoEnabled(true);

                    getSupportActionBar().setCustomView(R.layout.action_bar_custom);
                    getSupportActionBar().setDisplayShowCustomEnabled(true);
                    tvViewDay = (TextView) getSupportActionBar().getCustomView().findViewById(R.id.tvViewDay);
                } catch (Exception e) {
                    e.printStackTrace();
                    AlertDialogWindow.showMessage(this, R.string.titleError,
                                            R.string.errorActionBar, R.drawable.ic_error_outline, e);
                    finish();
                }

                // visualization of calendar
                gvCalendar = (GridViewSquareCells) findViewById(R.id.gvCalendar);

                // сохраняем в SharedPreferences текущий день
                final SPData spData = SPData.getInstance();
                spData.day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
                spData.month = Calendar.getInstance().get(Calendar.MONTH);
                spData.year = Calendar.getInstance().get(Calendar.YEAR);
                spData.saveViewDayMonthYear();

                // show calendar (current month)
                viewCalendar();

                // обработка клика по ячейке
                gvCalendar.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        clickByCalendarDay(view);
                    }
                });

                gvCalendar.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                        clickByCalendarDay(view);

                        // for future days options doesn't show
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(SPData.getInstance().year, SPData.getInstance().month, SPData.getInstance().day);

                        if (calendar.getTimeInMillis() > Calendar.getInstance().getTimeInMillis()) {
                            return false;
                        }
                        if (dialogDayOptions == null) {
                            dialogDayOptions = new DialogDayOptions();
                        }

                        dialogDayOptions.show(ActivityMain.getInstance().getFragmentManager(), "");
                        return false;
                    }
                });

                // write date of current day in action bar
                setTextViewDay(spData.day, spData.month, spData.year);

            } catch (Exception exp) {
                exp.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        menu.add(0, MENU_ABOUT, 0, R.string.menu_about);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        try {
            if (item.getItemId() == MENU_ABOUT) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

                // body
                View customBody = getLayoutInflater().inflate(R.layout.dialog_custom_body, parentNull);
                ((TextView) customBody.findViewById(R.id.tvAppName)).setText(R.string.app_name);
                String version = "";

                try {
                    PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                    version = getString(R.string.prefix_version) + " " + packageInfo.versionName;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ((TextView) customBody.findViewById(R.id.tvVersion)).setText(version);
                ((TextView) customBody.findViewById(R.id.tvCompany)).setText(R.string.company_info);
                alertDialog.setView(customBody);

                alertDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Set text - header for selected day in calendar
     * @param day number of day
     * @param month number of month
     * @param year year
     */
    private void setTextViewDay (int day, int month, int year) {

        String[] monthsLowerCase = getResources().getStringArray(R.array.months_lower_case);
        String[] daysOfWeek = getResources().getStringArray(R.array.days_of_week);

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        // number day of week in Calendar. Numeration begins from Sunday: Su - 1, Mo - 2, ...
        int dayOfWeekCalendar = calendar.get(Calendar.DAY_OF_WEEK);

        // number day of week in Calendar. Numeration begins from Monday: Mo - 1, Tu - 2, ...
        int dayOfWeek = dayOfWeekCalendar - 2 >= 0
                ? dayOfWeekCalendar - 2
                : COUNT_DAYS_IN_WEEK + dayOfWeekCalendar - 2;
        String str = daysOfWeek[dayOfWeek] + ", "
                + day + " "
                + monthsLowerCase[month] + " " + year;

        tvViewDay.setText(str);
    }

    /**
     * Visualisation calendar - month
     */
    private void viewCalendar () {
        try {
            String[] months = getResources().getStringArray(R.array.months);

            // наименование месяца
            TextView tvMonth = (TextView) findViewById(R.id.tvMonth);
            assert tvMonth != null;
            String text = months[SPData.getInstance().month] + " " + SPData.getInstance().year;
            tvMonth.setText(text);

            ArrayList<HashMap<String, Object>> arrayDays = getArrayDaysCalendar();
            DayCalendarAdapter adapter = new DayCalendarAdapter(this, arrayDays, SPData.getInstance().month, SPData.getInstance().year);

            assert gvCalendar != null;
            gvCalendar.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Filling array information by days in calendar
     * @return array with information by days
     */
    private ArrayList<HashMap<String, Object>> getArrayDaysCalendar () {

        ArrayList<HashMap<String, Object>> arrayDays = new ArrayList<>();

        // name month
        for (int i = 0; i < COUNT_DAYS_IN_WEEK; i++) {
            HashMap<String, Object> infoByDay = new HashMap<>();
            infoByDay.put(CommonData.KEY_DAY, i);
            arrayDays.add(infoByDay);
        }

        // set first day of selected month
        Calendar calendar = new GregorianCalendar(SPData.getInstance().year, SPData.getInstance().month, 1);

        // day of week for first day of month
        int firstDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        // number day of week in Calendar. Numeration begins from Monday: Mo - 1, Tu - 1, ...
        firstDayOfWeek = firstDayOfWeek - 2 >= 0
                ? firstDayOfWeek - 1
                : COUNT_DAYS_IN_WEEK + firstDayOfWeek - 1;

        // add empty cells which means last days from prev month
        for (int i = 1; i < firstDayOfWeek; i++) {
            arrayDays.add(null);
        }

        // count days in current month
        int daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        int serialNumber = 0; // number of day in menstruation cycle
        int menstruation = CommonData.VALUE_MENSTRUATION_ABSENCE;

        for (int day = 1; day <= daysInMonth; day++) {
            HashMap<String, Object> infoByDay = SPData.getInstance().getInfoByDay(day);
            if ((int)infoByDay.get(CommonData.KEY_MENSTRUATION) == CommonData.VALUE_MENSTRUATION_BEGIN) {
                serialNumber = 1;
                menstruation = CommonData.VALUE_MENSTRUATION_IS;
            } else if (day == 1 && (int) infoByDay.get(CommonData.KEY_SERIAL_NUMBER) > 0) {
                serialNumber = (int) infoByDay.get(CommonData.KEY_SERIAL_NUMBER);
                menstruation = (int) infoByDay.get(CommonData.KEY_MENSTRUATION) != CommonData.VALUE_MENSTRUATION_ABSENCE
                        ? CommonData.VALUE_MENSTRUATION_IS
                        : menstruation;
            } else if (serialNumber > 0) {
                serialNumber++;
            }

            // for continue numeration in the next month
            if (day == daysInMonth) {
                int nextMonth = SPData.getInstance().month < 11
                        ? SPData.getInstance().month + 1
                        : 0;
                int year = nextMonth == 0
                        ? SPData.getInstance().year + 1
                        : SPData.getInstance().year;
                if (serialNumber > 0) {
                    SPData.getInstance().setSerialNumberByDay(1, nextMonth, year, serialNumber + 1);
                } else {
                    SPData.getInstance().setSerialNumberByDay(1, nextMonth, year, 0);
                }
                int menstruationFirstDayNextMonth = (int) SPData.getInstance().get(CommonData.KEY_MENSTRUATION);
                if (menstruationFirstDayNextMonth == 0 && menstruationFirstDayNextMonth != menstruation) {
                    SPData.getInstance().setMenstruationByDay(1, nextMonth, year, menstruation);
                }
            }

            infoByDay.put(CommonData.KEY_SERIAL_NUMBER, serialNumber <= 60 ? serialNumber : 0);
            if ((int) infoByDay.get(CommonData.KEY_MENSTRUATION) == CommonData.VALUE_MENSTRUATION_END) {
                infoByDay.put(CommonData.KEY_MENSTRUATION, CommonData.VALUE_MENSTRUATION_IS);
                menstruation = CommonData.VALUE_MENSTRUATION_ABSENCE;
            } else if (serialNumber > 10 && menstruation == CommonData.VALUE_MENSTRUATION_IS) {
                infoByDay.put(CommonData.KEY_MENSTRUATION, CommonData.VALUE_MENSTRUATION_ABSENCE);
                menstruation = CommonData.VALUE_MENSTRUATION_ABSENCE;
            } else {
                infoByDay.put(CommonData.KEY_MENSTRUATION, menstruation);
            }
            arrayDays.add(infoByDay);
        }

        return arrayDays;
    }

    /**
     * Handler event onClick show next month
     * @param v ImageView
     */
    public void nextMonth (View v) {

        changeMonth(NEXT_MONTH);
    }

    /**
     * Handler event onCLick show prev month
     * @param v ImageView
     */
    public void prevMonth (View v) {

        changeMonth(PREV_MONTH);
    }

    /**
     * Switching between months
     * @param direction NEXT_MONTH or PREV_MONTH
     */
    private void changeMonth (String direction) {

        if (direction.equals(NEXT_MONTH)) {
            SPData.getInstance().month++;
            if (SPData.getInstance().month == 12) {
                SPData.getInstance().month = 0;
                SPData.getInstance().year++;
            }
        } else if (direction.equals(PREV_MONTH)) {
            SPData.getInstance().month--;
            if (SPData.getInstance().month == -1) {
                SPData.getInstance().month = 11;
                SPData.getInstance().year--;
            }
        }

        SPData.getInstance().saveViewDayMonthYear();
        viewCalendar();

        if (CommonData.selectedMonth == SPData.getInstance().month
                && CommonData.selectedYear == SPData.getInstance().year) {
            // если выбранный день находится в отображаемом месяце
            SPData.getInstance().day = CommonData.selectedDay;
            SPData.getInstance().saveViewDayMonthYear();
            setTextViewDay(CommonData.selectedDay, CommonData.selectedMonth, CommonData.selectedYear);
            // fillOptionsDay(CommonData.selectedDay, CommonData.selectedMonth, CommonData.selectedYear);
        } else if (Calendar.getInstance().get(Calendar.MONTH) == SPData.getInstance().month
                && Calendar.getInstance().get(Calendar.YEAR) == SPData.getInstance().year) {
            // если отображается текущий месяц
            SPData.getInstance().day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
            SPData.getInstance().saveViewDayMonthYear();
            setTextViewDay(Calendar.getInstance().get(Calendar.DAY_OF_MONTH), SPData.getInstance().month, SPData.getInstance().year);
            // fillOptionsDay(Calendar.getInstance().get(Calendar.DAY_OF_MONTH), SPData.getInstance().month, SPData.getInstance().year);
        } else {
            tvViewDay.setText("");
            // llOptions.setVisibility(View.GONE);
        }
    }

    /**
     * Handler event setOnItemClickListener of calendar (GridView)
     * @param view cell of GridView
     */
    private void clickByCalendarDay(View view) {
        try {
            // write date of selected day
            TextView tvDay = (TextView) view.findViewById(R.id.tvDay);
            int day = Integer.valueOf(tvDay.getText().toString());

            // if we clicked on the same day
            if (CommonData.selectedDay == day
                    && CommonData.selectedMonth == SPData.getInstance().month
                    && CommonData.selectedYear == SPData.getInstance().year) {
                return;
            }

            setTextViewDay(day, SPData.getInstance().month, SPData.getInstance().year);

            // select the cell border
            view.setBackgroundResource(R.drawable.background_border_click);

            // remove border from cell, which we clicked on
            View lastItemClick = null;
            if (CommonData.selectedDay > 0
                    && CommonData.selectedMonth == SPData.getInstance().month
                    && CommonData.selectedYear == SPData.getInstance().year) {

                lastItemClick = gvCalendar.findViewWithTag(CommonData.selectedDay);
            }
            if (lastItemClick != null) {
                lastItemClick.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),
                                                    android.R.color.white));
            }

            // remember the day
            SPData.getInstance().day = day;
            SPData.getInstance().saveViewDayMonthYear();

            CommonData.selectedDay = day;
            CommonData.selectedMonth = SPData.getInstance().month;
            CommonData.selectedYear = SPData.getInstance().year;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onApplyOptions() {
        viewCalendar();
    }

}
