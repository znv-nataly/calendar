package ru.dev.calendar;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

/**
 * Dialog window for view message
 */
class AlertDialogWindow {

    private static AlertDialog.Builder alertDialogBuilderMessage;

    /**
     * Visualisation dialog window with output text message and one button
     * @param context контекст
     * @param titleResource header of dialog window
     * @param messageResource text message
     * @param icon image for view in dialog window
     * @param exception text of exception will output with text message
     */
    static void showMessage(Context context, int titleResource, int messageResource, int icon, Exception exception) {

        try {
            if (alertDialogBuilderMessage == null || alertDialogBuilderMessage.getContext() != context) {
                alertDialogBuilderMessage = new AlertDialog.Builder(context);
                alertDialogBuilderMessage.setCancelable(false)
                        .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
            }

            alertDialogBuilderMessage.setTitle(context.getResources().getString(titleResource))
                    .setIcon(icon)
                    .setMessage(context.getResources().getString(messageResource) + ". " + (exception == null ? "" : exception.getMessage()));
            alertDialogBuilderMessage.show();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, R.string.errorOpenAlertDialog, Toast.LENGTH_LONG).show();
        }
    }


}
