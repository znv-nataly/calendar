package ru.dev.calendar;

/**
 * Common data for all classes in app
 */

class CommonData {

    final static String KEY_DAY = "day";
    final static String KEY_MENSTRUATION = "menstruation";
    final static String KEY_INTIM = "intim";
    final static String KEY_SERIAL_NUMBER = "serial_number";
    final static String KEY_NOTE = "note";

    final static int VALUE_MENSTRUATION_ABSENCE = 0;
    final static int VALUE_MENSTRUATION_BEGIN = 1;
    final static int VALUE_MENSTRUATION_END = 2;
    final static int VALUE_MENSTRUATION_IS = 3;

    final static int VALUE_INTIM_ABSENCE = 0;
    final static int VALUE_INTIM_WITH_CONDOM = 1;
    final static int VALUE_INTIM_WITHOUT_CONDOM = 2;

    static int selectedDay;
    static int selectedMonth;
    static int selectedYear;

    /**
     * Return date in format dd.mm.yyyy
     * @param day int number of day in calendar
     * @param month int number of month in calendar
     * @param year int year
     * @return String dd.mm.yyyy
     */
    static String getDayString (int day, int month, int year) {

        month++; // because numeration begin from 0, but in result string must be real number of month
        String dayString = day < 9 ? "0" + day : day + "";
        dayString += ".";
        dayString += month < 9 ? "0" + month : month + "";
        dayString += "." + year;

        return dayString;
    }
}
