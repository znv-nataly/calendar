package ru.dev.calendar;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Custom Adapter for view day of calendar
 */
class DayCalendarAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<HashMap<String, Object>> array;
    private Calendar calendar;
    private int viewMonth;
    private int viewYear;

    DayCalendarAdapter(Context context, ArrayList<HashMap<String, Object>> array, int viewMonth, int viewYear) {
        this.context = context;
        this.array = array;
        this.viewMonth = viewMonth;
        this.viewYear = viewYear;
        this.calendar = Calendar.getInstance();
    }

    @Override
    public int getCount() {
        return array.size();
    }

    @Override
    public HashMap<String, Object> getItem(int position) {
        if (!array.isEmpty()) {
            return array.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            GridViewSquareCells gridView = (GridViewSquareCells) parent;
            int size = gridView.getColumnWidth();

            if (convertView == null) {
                convertView = LayoutInflater.from(context).inflate(R.layout.day, parent, false);
                // adjust height and wight cells of calendar that its were equal
                convertView.setLayoutParams(new GridView.LayoutParams(size, size));
            }
            fillFieldsDay(convertView, position);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    /**
     * Filling fields inside cell of calendar
     * @param convertView cell of calendar
     * @param position number position in data array
     */
    private void fillFieldsDay (View convertView, int position) {

        HashMap<String, Object> infoByDay = getItem(position);

        if (infoByDay == null) {
            convertView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorBackground));
            return;
        }

        int day = (int) infoByDay.get(CommonData.KEY_DAY);

        if (infoByDay.size() == 1) {
            String[] daysOfWeek = ActivityMain.getInstance().getResources().getStringArray(R.array.days_of_week);
            String value = daysOfWeek[day];
            ((TextView) convertView.findViewById(R.id.tvDay)).setText(value);
            convertView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorBackground));
            return;
        }

        TextView tvDay = (TextView) convertView.findViewById(R.id.tvDay);
        tvDay.setText(day > 0 ? day + "" : "");
        if (calendar.get(Calendar.MONTH) == viewMonth && calendar.get(Calendar.YEAR) == viewYear && (calendar.get(Calendar.DAY_OF_MONTH)) == day) {
            // select currently day by border
            convertView.findViewById(R.id.rlToday).setBackgroundResource(R.drawable.background_border);
        } else if (CommonData.selectedDay == day && CommonData.selectedMonth == viewMonth && CommonData.selectedYear == viewYear) {
            // early selected day
            convertView.setBackgroundResource(R.drawable.background_border_click);
        } else {
            convertView.setBackgroundColor(ContextCompat.getColor(context, android.R.color.white));
        }

        if ((int) infoByDay.get(CommonData.KEY_SERIAL_NUMBER) > 0) {
            int serialNumber = (int) infoByDay.get(CommonData.KEY_SERIAL_NUMBER);
            ((TextView) convertView.findViewById(R.id.tvNumDay)).setText(String.valueOf(serialNumber));
            if ((int) infoByDay.get(CommonData.KEY_MENSTRUATION) != CommonData.VALUE_MENSTRUATION_ABSENCE) {
                convertView.findViewById(R.id.rlMenstruation).setBackgroundResource(R.color.colorMenstruation);
            }
        }
        if ((int) infoByDay.get(CommonData.KEY_INTIM) == CommonData.VALUE_INTIM_WITH_CONDOM) {
            convertView.findViewById(R.id.ivIntim).setVisibility(View.VISIBLE);
            convertView.findViewById(R.id.ivIntimacyWithCondom).setVisibility(View.VISIBLE);
        }
        if ((int) infoByDay.get(CommonData.KEY_INTIM) == CommonData.VALUE_INTIM_WITHOUT_CONDOM) {
            convertView.findViewById(R.id.ivIntim).setVisibility(View.VISIBLE);
        }

        if (!((String) infoByDay.get(CommonData.KEY_NOTE)).isEmpty()) {
            convertView.findViewById(R.id.ivNote).setVisibility(View.VISIBLE);
        }
        convertView.setTag(day);
    }

}
