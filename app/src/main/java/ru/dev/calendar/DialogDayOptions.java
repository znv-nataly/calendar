package ru.dev.calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;

import java.util.HashMap;

/**
 * Dialog window for setting options for selected day
 */

public class DialogDayOptions extends DialogFragment {

    CheckBox cbBegin;
    CheckBox cbEnd;

    Button btnCancel;
    Button btnSave;

    RadioGroup rgIntim;
    EditText etNote;


    // interface choose color for broadcast result into parent's activity
    interface OnDialogDayOptionsResultListener {

        void onApplyOptions ();
    }

    private OnDialogDayOptionsResultListener onComplete;

    @Override
    public void onAttach(Activity activity) {
        try {
            // attach activity, which implements interface OnDialogColorsResultListener
            super.onAttach(activity);
            onComplete = (OnDialogDayOptionsResultListener) getActivity();
        } catch (Exception e) {
            throw new ClassCastException("Calling Activity must implement OnDialogColorsResultListener");
        }
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        try {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
            final LayoutInflater inflater = getActivity().getLayoutInflater();
            final ViewGroup parentNull = null;
            final View view = inflater.inflate(R.layout.dialog_day_options, parentNull);

            alertDialogBuilder.setView(view);

            rgIntim = (RadioGroup) view.findViewById(R.id.rgIntim);

            cbBegin = (CheckBox) view.findViewById(R.id.cbBegin);
            cbEnd = (CheckBox) view.findViewById(R.id.cbEnd);
            etNote = (EditText) view.findViewById(R.id.etNote);

            // checked begin menstruation
            cbBegin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean checked = ((CheckBox) v).isChecked();
                    cbEnd.setEnabled(!checked);
                }
            });

            // checked end menstruation
            cbEnd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean checked = ((CheckBox) v).isChecked();
                    cbBegin.setEnabled(!checked);
                }
            });

            btnCancel = (Button) view.findViewById(R.id.btnCancel);
            btnSave = (Button) view.findViewById(R.id.btnSave);

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    // menstruation
                    if (cbBegin.isChecked()) {
                        SPData.getInstance().setMenstruationByDay(SPData.getInstance().day, CommonData.VALUE_MENSTRUATION_BEGIN);
                    } else if (cbEnd.isChecked()) {
                        SPData.getInstance().setMenstruationByDay(SPData.getInstance().day, CommonData.VALUE_MENSTRUATION_END);
                    } else {
                        SPData.getInstance().setMenstruationByDay(SPData.getInstance().day, CommonData.VALUE_MENSTRUATION_ABSENCE);
                    }

                    // intim
                    int intim = rgIntim.getCheckedRadioButtonId();
                    if (intim == R.id.rbWithCondom) {
                        SPData.getInstance().setIntimByDay(SPData.getInstance().day, CommonData.VALUE_INTIM_WITH_CONDOM);
                    } else if (intim == R.id.rbWithoutCondom) {
                        SPData.getInstance().setIntimByDay(SPData.getInstance().day, CommonData.VALUE_INTIM_WITHOUT_CONDOM);
                    } else {
                        SPData.getInstance().setIntimByDay(SPData.getInstance().day, CommonData.VALUE_INTIM_ABSENCE);
                    }

                    // note
                    String textNote = etNote.getText().toString();
                    SPData.getInstance().setNoteByDay(SPData.getInstance().day, textNote);

                    onComplete.onApplyOptions();
                    dismiss();
                }
            });

            fillOptionsDay();

            return alertDialogBuilder.create();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Filling options for selected day
     */
    private void fillOptionsDay() {

        HashMap<String, Object> infoByDay = SPData.getInstance().getInfoByDay(
                SPData.getInstance().day,
                SPData.getInstance().month,
                SPData.getInstance().year);

        // begin / end menstruation
        int valueMenstruation = (int) infoByDay.get(CommonData.KEY_MENSTRUATION);

        if (valueMenstruation == CommonData.VALUE_MENSTRUATION_BEGIN) {
            cbBegin.setChecked(true);
            cbBegin.setEnabled(true);
            cbEnd.setChecked(false);
            cbEnd.setEnabled(false);
        } else if (valueMenstruation == CommonData.VALUE_MENSTRUATION_END) {
            cbBegin.setChecked(false);
            cbBegin.setEnabled(false);
            cbEnd.setChecked(true);
            cbEnd.setEnabled(true);
        } else {
            cbBegin.setChecked(false);
            cbBegin.setEnabled(true);
            cbEnd.setChecked(false);
            cbEnd.setEnabled(true);
        }

        // intimacy
        int valueIntim = (int) infoByDay.get(CommonData.KEY_INTIM);
        if (valueIntim == CommonData.VALUE_INTIM_WITH_CONDOM) {
            // with condom
            rgIntim.check(R.id.rbWithCondom);
        } else if (valueIntim == CommonData.VALUE_INTIM_WITHOUT_CONDOM) {
            // without condom
            rgIntim.check(R.id.rbWithoutCondom);
        } else {
            // absent
            rgIntim.check(R.id.rbAbsence);
        }

        String note = (String) infoByDay.get(CommonData.KEY_NOTE);
        etNote.setText(note);
    }
}
