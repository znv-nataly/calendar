package ru.dev.calendar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.util.AttributeSet;
import android.widget.GridView;

import java.lang.reflect.Field;

/**
 * GridView with square cells
 */
public class GridViewSquareCells extends GridView {

    public GridViewSquareCells(Context context) {
        super(context);
    }

    public GridViewSquareCells(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GridViewSquareCells(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @SuppressLint("NewApi")
    @Override
    public int getColumnWidth() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            return super.getColumnWidth();
        else {
            try {
                Field field = GridView.class.getDeclaredField("mColumnWidth");
                field.setAccessible(true);
                Integer value = (Integer) field.get(this);
                field.setAccessible(false);

                return value;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        if (getLayoutParams().height == ActionBar.LayoutParams.WRAP_CONTENT) {
            // set height GidView by content
            heightMeasureSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >>  2, MeasureSpec.AT_MOST);
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
