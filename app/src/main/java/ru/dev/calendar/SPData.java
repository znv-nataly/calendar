package ru.dev.calendar;
import android.content.SharedPreferences;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Class for save in SharedPreferences month and year, which are viewed on screen now
 */
class SPData {

    private final String SP_DAY = "sp_day";
    private final String SP_MONTH = "sp_month";
    private final String SP_YEAR = "sp_year";

    private final static String PREFIX_MENSTRUATION = CommonData.KEY_MENSTRUATION + "_";
    private final static String PREFIX_INTIM = CommonData.KEY_INTIM + "_";
    private final static String PREFIX_SERIAL_NUMBER = CommonData.KEY_SERIAL_NUMBER + "_";
    private final static String PREFIX_NOTE = CommonData.KEY_NOTE + "_";

    int day;
    int month;
    int year;

    private static SPData instance;

    static SPData getInstance() {
        if (instance == null) {
            instance = new SPData();
        }
        return instance;
    }

    private SPData () {

        // read SharedPreferences, if there are nothing, than write current month and year
        SharedPreferences sp = ActivityMain.getInstance().getPreferences(ActivityMain.MODE_PRIVATE);

        final int INVALID_VALUE = -1;
        int day = sp.getInt(SP_DAY, INVALID_VALUE);
        int month = sp.getInt(SP_MONTH, INVALID_VALUE);
        int year = sp.getInt(SP_YEAR, INVALID_VALUE);

        if (day == INVALID_VALUE || month == INVALID_VALUE || year == INVALID_VALUE) {
            SharedPreferences.Editor editor = sp.edit();

            if (day == INVALID_VALUE) {
                // current day
                day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
                editor.putInt(SP_DAY, day);
            }
            if (month == INVALID_VALUE) {
                // current month
                month = Calendar.getInstance().get(Calendar.MONTH);
                editor.putInt(SP_MONTH, month);
            }
            if (year == INVALID_VALUE) {
                // current year
                year = Calendar.getInstance().get(Calendar.YEAR);
                editor.putInt(SP_YEAR, year);
            }
            editor.apply();
        }

        this.month = month;
        this.year = year;
    }

    /**
     * Save value viewed month and year in SharedPreferences
     */
    void saveViewDayMonthYear () {

        SharedPreferences sp = ActivityMain.getInstance().getPreferences(ActivityMain.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt(SP_DAY, day);
        editor.putInt(SP_MONTH, month);
        editor.putInt(SP_YEAR, year);
        editor.apply();
    }

    /**
     * Get data by day
     * @param day number day in calendar
     * @return information by day
     */
    HashMap<String, Object> getInfoByDay(int day) {

        return getInfoByDay(day, month, year);
    }

    /**
     * Get data by day
     * @param day number day in calendar
     * @param month number month in calendar
     * @param year year
     * @return HaspMap
     */
    HashMap<String, Object> getInfoByDay(int day, int month, int year) {

        HashMap<String, Object> result = new HashMap<>();

        SharedPreferences sp = ActivityMain.getInstance().getPreferences(ActivityMain.MODE_PRIVATE);

        String dayString = CommonData.getDayString(day, month, year);

        String keyMenstruation = PREFIX_MENSTRUATION + dayString;
        String keyIntim = PREFIX_INTIM + dayString;
        String keySerialNumber = PREFIX_SERIAL_NUMBER + dayString;
        String keyNote = PREFIX_NOTE + dayString;

        result.put(CommonData.KEY_DAY, day);
        result.put(CommonData.KEY_MENSTRUATION,
                sp.getInt(keyMenstruation, CommonData.VALUE_MENSTRUATION_ABSENCE));
        result.put(CommonData.KEY_INTIM,
                sp.getInt(keyIntim, CommonData.VALUE_INTIM_ABSENCE));
        result.put(CommonData.KEY_SERIAL_NUMBER,
                sp.getInt(keySerialNumber, 0));
        result.put(CommonData.KEY_NOTE, sp.getString(keyNote, ""));

        return result;
    }

    /**
     * Save begin / end / absent menstruation
     * @param day number day in calendar
     * @param month number month in calendar
     * @param year year
     * @param menstruation [ MENSTRUATION_BEGIN, MENSTRUATION_END ]
     */
    void setMenstruationByDay(int day, int month, int year, Integer menstruation) {

        SharedPreferences sp = ActivityMain.getInstance().getPreferences(ActivityMain.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        String dayString = CommonData.getDayString(day, month, year);
        if (menstruation == CommonData.VALUE_MENSTRUATION_ABSENCE) {
            editor.remove(PREFIX_MENSTRUATION + dayString);
        } else {
            editor.putInt(PREFIX_MENSTRUATION + dayString, menstruation);
        }
        editor.apply();
    }

    /**
     * Save begin / end / absent menstruation
     * month and year - view in calendar now
     * @param day number day in calendar
     * @param menstruation [ MENSTRUATION_BEGIN, MENSTRUATION_END, MENSTRUATION_ABSENT ]
     */
    void setMenstruationByDay(int day, Integer menstruation) {

        setMenstruationByDay(day, month, year, menstruation);
    }

    /**
     * Save kind of intimacy
     * @param day number day in calendar
     * @param intim int [ INTIM_ABSENCE, INTIM_WITH_CONDOM, INTIM_WITHOUT_CONDOM ]
     */
    void setIntimByDay(int day, Integer intim) {

        SharedPreferences sp = ActivityMain.getInstance().getPreferences(ActivityMain.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        String dayString = CommonData.getDayString(day, sp.getInt(SP_MONTH, 0), sp.getInt(SP_YEAR, 0));
        if (intim == CommonData.VALUE_INTIM_ABSENCE) {
            editor.remove(PREFIX_INTIM + dayString);
        } else {
            editor.putInt(PREFIX_INTIM + dayString, intim);
        }
        editor.apply();
    }

    /**
     * Save note
     * @param day number day in calendar
     * @param note string text note
     */
    void setNoteByDay(int day, String note) {

        SharedPreferences sp = ActivityMain.getInstance().getPreferences(ActivityMain.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        String dayString = CommonData.getDayString(day, sp.getInt(SP_MONTH, 0), sp.getInt(SP_YEAR, 0));
        if (note.isEmpty()) {
            editor.remove(PREFIX_NOTE + dayString);
        } else {
            editor.putString(PREFIX_NOTE + dayString, note);
        }
        editor.apply();
    }

    /**
     * Save serial number for day in menstruation cycle
     * @param day number day in calendar
     * @param month number month in calendar
     * @param year year
     * @param serialNumber serial number of day in menstruation cycle
     */
    void setSerialNumberByDay(int day, int month, int year, Integer serialNumber) {

        SharedPreferences sp = ActivityMain.getInstance().getPreferences(ActivityMain.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        String dayString = CommonData.getDayString(day, month, year);
        if (serialNumber > 0) {
            editor.putInt(PREFIX_SERIAL_NUMBER + dayString, serialNumber);
        } else {
            editor.remove(PREFIX_SERIAL_NUMBER+ dayString);
        }
        editor.apply();
    }


    /**
     * Get int value field from SharedPreferences
     * @param field CommonData.KEY_
     * @param day number of day in calendar
     * @param  month number of month
     * @param  year number of year
     * @return Object value field from SharedPreferences
     */
    private Object get (String field, int day, int month, int year) {

        SharedPreferences sp = ActivityMain.getInstance().getPreferences(ActivityMain.MODE_PRIVATE);
        String keyField = field + "_" + CommonData.getDayString(day, month, year);
        return field.equals(CommonData.KEY_NOTE) ? sp.getString(keyField, "") : sp.getInt(keyField, 0);
    }


    /**
     * Get int value field from SharedPreferences
     * @param field CommonData.KEY_
     * @return Object value field from SharedPreferences
     */
    Object get (String field) {

        return get(field, day, month, year);
    }

}
